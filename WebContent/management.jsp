<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>

<script type="text/javascript">
<!--

function disp1(){
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('選択されたアカウントを停止します')){
	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
	}
	// 「キャンセル」時の処理終了
}

function disp2(){
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('選択されたアカウントを復活します')){
	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{
		window.alert('キャンセルされました'); // 警告ダイアログを表示
	}
	// 「キャンセル」時の処理終了
}

// -->
</script>

</head>
<body>
	<h2>ユーザー管理画面</h2>
		<a href="signup">ユーザー新規登録</a>&ensp;<a href="./">ホームへ戻る</a>
	<br /><br />
<div class="usersinfomation">
<table  border="1">
<tr>
<th>ユーザー名</th>
<th>ログインID</th>
<th>支店名</th>
<th>部署・役職</th>
<th>停止･復活</th>
<th>アカウントの状態</th>
<th>編集</th>
</tr>
<c:forEach items="${UserInfomation}" var="user">
<tr>
<td><c:out value="${user.name}" /></td>
<td><c:out value="${user.account}" /></td>
<td><c:out value="${user.branch_name}" /></td>
<td><c:out value="${user.department_name}" /></td>
<td>
<c:if test="${loginUser.id != user.id}">
<form action="management" method="post">
<c:if test="${user.is_deleted == 0}">
<input type="hidden" name="is_deleted" value="1"/>
<input type="hidden" name="id" value="${user.id}"/>
<input type="submit" value="停止" onClick="disp1()"/>
</c:if>
<c:if test="${user.is_deleted == 1}">
<input type="hidden" name="is_deleted" value="0"/>
<input type="hidden" name="id" value="${user.id}"/>
<input type="submit" value="復活" onClick="disp2()"/>
</c:if>
</form>
</c:if>
<c:if test="${loginUser.id == user.id}">
ログイン中
</c:if>
</td>
<td><c:if test="${user.is_deleted == 0}">使用可能</c:if><c:if test="${user.is_deleted == 1}"><font color="red">アカウント停止中</font></c:if></td>
<td><a href="settings?id=${user.id}">編集</a></td>
</tr>
</c:forEach >
</table>
</div>
	<div class="copyright"> Copyright(c)Kondo Miyuki</div>
</body>
</html>