<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/login.css" rel="stylesheet" type="text/css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ログイン</title>
</head>
<body style="height: 650px; margin: 0px;">
	<div class="backing">
	<div class="login_maincontents">
    <font color="#555555"><h1>社内掲示板</h1></font>
  		<div class="errorMessages">
			<c:if test="${ not empty errorMessages }">
				<c:forEach items="${errorMessages}" var="messages">
					<c:out value="${messages}"/>
				</c:forEach>
	            <c:remove var="errorMessages" scope="session"/>
			</c:if>
			</div>
        <form action="login" method="post"><br />
        <div class="loginform">
			<input type="text" placeholder="ログインID" name="account" value="${account}" id="account"/> <br />
            <input name="password"  type="password" id="password" placeholder="パスワード"/> <br />
			<br />
			<input type="submit" value="ログイン"class="square_btn">

            <br />
		</div>
        </form>
        <br />
        <div class="copyright"> Copyright(c)Kondo Miyuki</div>
        </div>
	</div>
</body>


</html>