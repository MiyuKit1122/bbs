<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー情報編集</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body class="setting_body">
    <div class="main-contents">

        <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
                <ul>
                    <c:forEach items="${errorMessages}" var="messages">
                        <c:out value="${messages}" />
                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session"/>
        </c:if>

        <h2>ユーザー情報編集画面</h2>

        <form action="settings" method="post"><br />
            <input name="id" value="${editUser.id}" id="id" type="hidden"/>
            <label for="name">ユーザー名</label>
            <input name="name" value="${editUser.name}" id="name"/><br />

            <label for="account">ログインID</label>
            <input name="account" value="${editUser.account}" id="account"/><br />

            <label for="password">パスワード</label>
            <input name="password1" type="password" id="password1"/> <br />

             <label for="password">確認用パスワード</label>
            <input name="password2" type="password" id="password2"/> <br />

			<label for="branches">勤務地</label>
			<select name = "branch_id">
			<c:forEach items="${branches}" var ="branch">
				<option value="${branch.id}" id="branch">${branch.name}</option>
			</c:forEach>
			</select><br />

			<label for="departments">役職・部署</label>
			<select name = "department_id">
			<c:forEach items="${departments}" var ="department">
				<option value="${department.id}" id="department">${department.name}</option>
			</c:forEach>
			</select><br /><br />

            <input type="submit" value="完了" /><br />
            <br /><a href="management">ユーザー管理へ戻る</a>
        </form>
        <div class="copyright"> Copyright(c)Kondo Miyuki</div>
    </div>
</body>
</html>