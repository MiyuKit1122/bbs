<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link href="./css/style.css" rel="stylesheet" type="text/css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録</title>
</head>
<body>
    <h2>ユーザー新規登録</h2>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="messages">
                           <li><c:out value="${messages}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
                <c:remove var="errorMessages" scope="session" />
            	<form action="signup" method="post">
                <br />
                <label for="name">ユーザー名</label>
				<input name="name"value="${User.name}" id="name" /> <br />

                <label for="account">ログインID</label>
                <input name="account" value="${User.account}" id="account" /><br />

                <label for="password1">パスワード</label>
                <input name="password1" type="password" value="${User.password}" id="password1" /> <br />

                <label for="password2">確認パスワード</label>
                <input name="password" type="password" value="${User.password}" id="password2" /> <br />

                <label for="branches">勤務地</label>
				<select name = "branch_id">
					<c:forEach items="${branches}" var ="branch">
						<option value="${branch.id}" id="branch">${branch.name}</option>
					</c:forEach>
				</select><br />

				<label for="departments">役職・部署</label>
				<select name = "department_id">
					<c:forEach items="${departments}" var ="department">
						<option value="${department.id}" id="department">${department.name}</option>
					</c:forEach>
				</select> <br />

                <br /><input type="submit" value="登録" /> <br />
                <br /><a href="management">ユーザー管理へ戻る</a>
            </form>
            <div class="copyright">Copyright(c)Kondo Miyuki</div>
        </div>
</body>
</html>