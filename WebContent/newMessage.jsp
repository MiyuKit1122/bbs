<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>

<head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
</head>
<body>
<h2>新規投稿画面</h2>
<div class="errorMessages">
	<c:if test="${ not empty errorMessages }">
		<c:forEach items="${errorMessages}" var="messages">
			<c:out value="${messages}"/><br/>
		</c:forEach>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>
</div>

<br />

<div class="form-area">
<form action="newMessage" method="post">
	<label for="title">◆タイトル<br />（30文字以内で入力してください）</label><br />
	<input name="title"  value="${User.title}" id="title" /> <br />
	<br />◆本文<br />（1000文字以内で入力してください）<br />
	<textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
	<br />
	<br />
	<label for="category">◆カテゴリー<br />（10文字以内で入力してください）</label><br />
	<input name="category"  value="${User.category}" id="category" /> <br />
	<br />
	<input type="submit" value="投稿する">
</form>
</div>

<br />
<a href="./">ホームへ戻る</a>
<div class="copyright"> Copyright(c)Kondo Miyuki</div>

</body>
</html>
