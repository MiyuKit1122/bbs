<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./css/top.css" rel="stylesheet" type="text/css">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>トップ画面</title>
</head>
<body class="top_body">
	<div class="top_maincontents">
	<div class="header">
		<h2>トップ画面</h2>
	<div class="errorMessages">
			<c:if test="${ not empty errorMessages }">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
				</c:forEach>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
	</div>
	</div>
	<div class="form-area">
		<form method="get" action="./">
			<input type="text" name="category" placeholder="カテゴリを入力してください"/>
			<input type="submit" value="検索" class="square_btn35">
		</form>
		<form method="get" action="./" >
			<input type="date" name="start" /> ～
			<input type="date" name="end" />
			<input type="submit" value="検索" class="square_btn35">
		</form>
			<a href="newMessage"  class="square_btn35">新規投稿</a>&ensp;
			<c:if test="${loginUser.department_id == 1}">
			<a href="management"  class="square_btn35">ユーザー管理</a>
			</c:if>

		<c:if test="${ not empty loginUser }">
			<a href="logout"class="square_btn35">ログアウト</a><br />
		</c:if>

	</div>


	<div class="newMessage">
		<c:forEach items="${newMessage}" var="messages">
			<div class="newMessage">
			<div class="title">
				<br />
				<span class="title"><label for="title">◆タイトル</label><br /><c:out value="${messages.title}" /></span>
			</div>
				<br />
			<div class="text"><label for="text">◆本文</label><br /><c:out value="${messages.text}" /></div>
				<br />
			<div class="category"><label for="category">カテゴリー：</label><c:out value="${messages.category}" /></div>
			<div class="nameanddate"><label for="name">投稿者：</label><c:out value="${messages.user_name}" />&ensp;
				 <label for="created_date">投稿日時：</label><fmt:formatDate value="${messages.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />&ensp;</div>
				 <form action="DeleteContribution" method="post">
				 <input type="hidden" name="contribution_id" value="${messages.id}"/>
				 <input type="submit" value="投稿を削除">
				 </form>
				 <br />

				<label for="comment">◆コメント</label>
				<c:forEach items="${Comment}" var="comment">
						<c:if test="${comment.contribution_id == messages.id}">
					<div class="comment"><c:out value="${comment.comment}" /></div>
					<div class="nameanddate"><label for="name">投稿者：</label><c:out value="${comment.user_name}" />&ensp;</div>
						<label for="date">投稿日時：</label>
						<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss"/>&ensp;
						<form action="DeleteComment" method="post">
						<input type="hidden" name="comment_id" value="${comment.id}"/>
						<input type="submit" value="削除">
						</form>
						<br />
						</c:if>
				</c:forEach>
			</div>

			<div class="form-area">
        	<form action="comments" method="post">
        		<br />【コメント】<br />（500文字以内で入力してください）<br />
            	<textarea maxlength="500" name="comment" cols="50" rows="5" class="comment-box"></textarea>
				<input type="hidden" name="contribution_id" value="${messages.id}">
				<br /><input type="submit" value="コメントする"><br />
				<br />
			</form>
			</div>
		</c:forEach>
	</div>
			<br />
            <div class="copyright"> Copyright(c)Kondo Miyuki</div>
    </div>
    </body>
</html>

