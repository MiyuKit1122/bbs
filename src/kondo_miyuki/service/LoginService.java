package kondo_miyuki.service;

import static kondo_miyuki.utils.CloseableUtil.*;
import static kondo_miyuki.utils.DBUtil.*;

import java.sql.Connection;

import kondo_miyuki.bean.User;
import kondo_miyuki.dao.UserDao;
import kondo_miyuki.utils.CipherUtil;

public class LoginService {

    public User login(String account , String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, account, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}