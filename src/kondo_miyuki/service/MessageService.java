package kondo_miyuki.service;

import static kondo_miyuki.utils.CloseableUtil.*;
import static kondo_miyuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kondo_miyuki.bean.Message;
import kondo_miyuki.bean.UserMessage;
import kondo_miyuki.dao.MessageDao;
import kondo_miyuki.dao.UserMessageDao;

public class MessageService {

//    public void register(Message message) {


    	private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String start, String end, String category) {

    		Connection connection = null;
    	try {
    		connection = getConnection();

    		UserMessageDao messageDao = new UserMessageDao();
    		List<UserMessage> date = messageDao.getUserMessages(connection, start,end,category);

    		commit(connection);

        		return date;
    		} catch (RuntimeException e) {
    			rollback(connection);
    			throw e;
    		} catch (Error e) {
    			rollback(connection);
    		throw e;
    		} finally {
    			close(connection);
    		}
    }

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(int id){

		Connection connection = null;
	try {
		connection = getConnection();

		MessageDao messageDao = new MessageDao();
		messageDao.delete(connection, id);

		commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
		throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> serch(String start, String end) {

		Connection connection = null;
	try {
		connection = getConnection();

		UserMessageDao messageDao = new UserMessageDao();
		messageDao.getUserMessages(connection, "start", "end","category");

		commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
		throw e;
		} finally {
			close(connection);
		}
	return null;
	}
}
