package kondo_miyuki.service;

import static kondo_miyuki.utils.CloseableUtil.*;
import static kondo_miyuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kondo_miyuki.bean.Comment;
import kondo_miyuki.dao.CommentDao;
import kondo_miyuki.dao.CommentOutDao;

	public class CommentService {
	    private static final int LIMIT_NUM = 1000;

		public void register(Comment comment) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            CommentDao commentDao = new CommentDao();
	            commentDao.insert(connection, comment);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

		 public List<Comment> getComment() {

	    		Connection connection = null;
	    	try {
	    		connection = getConnection();

	    		CommentOutDao commentDao = new CommentOutDao();
	    		List<Comment> ret = commentDao.getComment(connection, LIMIT_NUM);

	    		commit(connection);

	        		return ret;
	    		} catch (RuntimeException e) {
	    			rollback(connection);
	    			throw e;
	    		} catch (Error e) {
	    			rollback(connection);
	    		throw e;
	    		} finally {
	    			close(connection);
	    		}
	    }

		 public void delete(int id){

	    		Connection connection = null;
	    	try {
	    		connection = getConnection();

	    		CommentDao commentDao = new CommentDao();
	    		commentDao.delete(connection, id);

	    		commit(connection);

	    		} catch (RuntimeException e) {
	    			rollback(connection);
	    			throw e;
	    		} catch (Error e) {
	    			rollback(connection);
	    		throw e;
	    		} finally {
	    			close(connection);
	    		}
	    }
	}


