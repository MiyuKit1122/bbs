package kondo_miyuki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kondo_miyuki.service.MessageService;

/**
 * Servlet implementation class DeleteContributionServlet
 */
@WebServlet("/DeleteContribution")
public class DeleteContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int contribution_id = Integer.parseInt(request.getParameter("contribution_id"));
    	new MessageService().delete(contribution_id);
        response.sendRedirect("./");
	}
}
