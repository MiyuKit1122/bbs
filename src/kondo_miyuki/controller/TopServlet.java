package kondo_miyuki.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.Comment;
import kondo_miyuki.bean.UserMessage;
import kondo_miyuki.service.CommentService;
import kondo_miyuki.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

//        List<UserMessage> date = new MessageService().getMessage("start", "end");
        String start = request.getParameter("start");
        String end = request.getParameter("end");

        if (start == null || StringUtils.isEmpty(start)){
        	start = "2018-05-01";
        }
        if (end == null || StringUtils.isEmpty(end)) {

        	Date today = new Date();
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	end = sdf.format(today);
        }
        end = end + " 23:59:59";


        String category = request.getParameter("category");

        List<UserMessage> messages = new MessageService().getMessage(start,end,category);
        request.setAttribute("newMessage", messages);

        List<Comment> comment = new CommentService().getComment();
        request.setAttribute("Comment", comment);
        request.getRequestDispatcher("/top.jsp").forward(request, response);

    }
}

