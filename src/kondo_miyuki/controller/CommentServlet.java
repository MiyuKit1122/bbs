package kondo_miyuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.Comment;
import kondo_miyuki.bean.User;
import kondo_miyuki.service.CommentService;

@WebServlet(urlPatterns = { "/comments" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

   	 HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();
        if (isValid(request, comments) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
           // comments.setId(comments.getId());
            comment.setComment(request.getParameter("comment"));
            comment.setUser_id(user.getId());
            String contribution_id = request.getParameter("contribution_id");
            comment.setContribution_id(Integer.parseInt(contribution_id));

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
        	session.setAttribute("errorMessages", comments);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comments) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
            comments.add("コメントを入力してください");
        }else{
        	if (comment.length() > 500) {
		    	comments.add("コメントを500文字以内で入力してください");
		    }
        }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}