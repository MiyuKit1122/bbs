package kondo_miyuki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kondo_miyuki.bean.User;
import kondo_miyuki.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> userList = new UserService().getAllUser();

        //ユーザー管理用のリストを画面にセットし、管理画面を表示する
        request.setAttribute("UserInfomation", userList);
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	//アカウント停止・復活機能
    	int is_deleted = Integer.parseInt(request.getParameter("is_deleted"));
    	int id = Integer.parseInt(request.getParameter("id"));
    	new UserService().isDeleted(is_deleted, id);

    	response.sendRedirect("./management");

    }
}
