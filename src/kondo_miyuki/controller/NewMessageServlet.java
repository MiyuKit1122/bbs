package kondo_miyuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.Message;
import kondo_miyuki.bean.User;
import kondo_miyuki.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setTitle(request.getParameter("title"));
            message.setText(request.getParameter("message"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");

        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newMessage");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messagesList)
    		throws IOException, ServletException,ServletException{

    	//タイトルのバリデーションメッセージ
    	String title = request.getParameter("title");

        //タイトルが未入力の場合
        if (StringUtils.isEmpty(title) == true) {
            messagesList.add("タイトルを入力してください");
        } else {
	        //タイトルが30字以上の場合
	        if (title.length() > 30) {
	        	messagesList.add("タイトルを30文字以内で入力してください");
	        }
        }

        //メッセージのバリデーションメッセージ
        String message = request.getParameter("message");

        //本文が未入力の場合
        if (StringUtils.isEmpty(message) == true) {
            messagesList.add("本文を入力してください");
        } else {
		    //本文が1000字以上の場合
		    if (message.length() > 1000) {
		    	messagesList.add("本文を1000文字以内で入力してください");
		    }
        }

      //カテゴリーのバリデーションメッセージ
        String category = request.getParameter("category");

        //カテゴリーが未入力の場合
        if (StringUtils.isEmpty(category) == true) {
            messagesList.add("カテゴリーを入力してください");
        } else {
	        //カテゴリーが10字以上の場合
	        if (category.length() > 10) {
	        	messagesList.add("カテゴリーを10文字以内で入力してください");
	        }
        }

        if (messagesList.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}