package kondo_miyuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.User;
import kondo_miyuki.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	String account = request.getParameter("account");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(account, password);

        List<String> messagesList = new ArrayList<String>();

        HttpSession session = request.getSession();

        if (isValid(request, messagesList) == true) {
//        if (user != null) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました");

            session.setAttribute("errorMessages", messages);
            response.sendRedirect("login");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messagesList)
    		throws IOException, ServletException,ServletException{

    	String account = request.getParameter("account");
        String password = request.getParameter("password");

        //ログインIDが未入力の場合
    	if (StringUtils.isEmpty(account) == true) {
            messagesList.add("ログインに失敗しました");
        }
    	//パスワードが未入力の場合
    	if (StringUtils.isEmpty(password) == true) {
            messagesList.add("ログインに失敗しました");
        }
    	if (messagesList.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}