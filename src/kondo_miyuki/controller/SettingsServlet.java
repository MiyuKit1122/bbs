package kondo_miyuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.Branch;
import kondo_miyuki.bean.Department;
import kondo_miyuki.bean.User;
import kondo_miyuki.exception.NoRowsUpdatedRuntimeException;
import kondo_miyuki.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        //セッションよりログインユーザーの情報を取得
    	int user_id = Integer.parseInt(request.getParameter("id"));
        //ユーザー情報のidを元にDBからユーザー情報取得
        User editUser = new UserService().getUser(user_id);
        request.setAttribute("editUser", editUser);

        List<Branch> branches = new UserService().branches();
        session.setAttribute("branches", branches);

        List<Department> department = new UserService().department();
        session.setAttribute("departments", department);

        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    	protected void doPost(HttpServletRequest request,
    			HttpServletResponse response) throws ServletException, IOException {

	    List<String> messages = new ArrayList<String>();
	    HttpSession session = request.getSession();
	    User editUser = getEditUser(request);

	    if (isValid(request, messages) == true) {

	        try {
	            new UserService().update(editUser);
	        } catch (NoRowsUpdatedRuntimeException e) {
	            messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
	            session.setAttribute("errorMessages", messages);
	            request.setAttribute("editUser", editUser);
	            request.getRequestDispatcher("settings.jsp").forward(request, response);
	            return;
	        }

	        session.setAttribute("loginUser", editUser);
	        response.sendRedirect("./management");

	    } else {
	        session.setAttribute("errorMessages", messages);
	        request.setAttribute("editUser", editUser);
	        request.getRequestDispatcher("setting.jsp").forward(request, response);
	    }
	}
    //ユーザー情報編集
	private User getEditUser(HttpServletRequest request)
	        throws IOException, ServletException {

	    User editUser = new User();
	    editUser.setId(Integer.parseInt(request.getParameter("id")));
	    editUser.setAccount(request.getParameter("account"));
	    editUser.setName(request.getParameter("name"));
	    editUser.setBranch_id(request.getParameter("branch_id"));
	    editUser.setDepartment_id(request.getParameter("department_id"));
	    editUser.setPassword(request.getParameter("password1"));
	    return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
	    String account = request.getParameter("account");
	    String password = request.getParameter("password1");
	    String chkpassword = request.getParameter("password2");

	    if (StringUtils.isEmpty(name) == true) {
	        messages.add("ユーザー名を入力してください");
	    }
	    if (StringUtils.isEmpty(account) == true) {
	        messages.add("ログインIDを入力してください");
	    }
	    if (StringUtils.isEmpty(password) == false) {
	    	if (password.equals(chkpassword)) {
	    		messages.add("パスワードが一致しません");
	    	}
	    }
	    // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
	    if (messages.size() == 0) {
	        return true;
	    } else {
	        return false;
	    }

	}

}
