package kondo_miyuki.dao;

import static kondo_miyuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.Branch;
import kondo_miyuki.bean.Department;
import kondo_miyuki.bean.User;
import kondo_miyuki.exception.NoRowsUpdatedRuntimeException;
import kondo_miyuki.exception.SQLRuntimeException;

public class UserDao {

	//ユーザー新規登録
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranch_id());
            ps.setString(5, user.getDepartment_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ユーザー管理
    public List<User> getAllUser(Connection connection) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
        	sql.append("users.id,");
            sql.append("users.account,");
            sql.append("users.name as user_name,");
            sql.append("users.is_deleted,");
            sql.append("branches.name as branch_name,");
            sql.append("departments.name as department_name");
            sql.append(" FROM users");
            sql.append(" INNER JOIN branches");
            sql.append(" ON users.branch_id = branches.id");
            sql.append(" INNER JOIN departments");
            sql.append(" ON users.department_id = departments.id");


            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> userList = toAllUserList(rs);
            return userList;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //なにか用SQL文
	public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	//ログイン用
	public User getUser(Connection connection, String account,
	        String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE (account = ? ) AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, account);
	        ps.setString(2, password);
//	        ps.setString(3, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	//ユーザー管理画面用(is_deletedも、アカウント停止・復活のためにとってくる)
	private List<User> toAllUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String account = rs.getString("account");
	            String user_name = rs.getString("user_name");
	            String branch_name = rs.getString("branch_name");
	            String department_name = rs.getString("department_name");
	            int is_deleted = rs.getInt("is_deleted");

	            User user = new User();
	            user.setId(id);
	            user.setAccount(account);
	            user.setName(user_name);
	            user.setBranch_name(branch_name);
	            user.setDepartment_name(department_name);
	            user.setIs_deleted(is_deleted);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String account = rs.getString("account");
	            String password = rs.getString("password");
	            String name = rs.getString("name");
	            String branch_id = rs.getString("branch_id");
	            String department_id = rs.getString("department_id");

	            User user = new User();
	            user.setId(id);
	            user.setAccount(account);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranch_id(branch_id);
	            user.setDepartment_id(department_id);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	//ユーザー情報編集画面プルダウン用、支店名リスト
	 public List<Branch> branches(Connection connection) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
        	sql.append("*");
            sql.append(" FROM branches");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> branches = tobranches(rs);
            return branches;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<Branch> tobranches(ResultSet rs) throws SQLException {

	    List<Branch> bran = new ArrayList<>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String name = rs.getString("name");

	            Branch branch = new Branch();
	            branch.setId(id);
	            branch.setName(name);


	            bran.add(branch);
	        }
	        return bran;
	    } finally {
	        close(rs);
	    }
	}

	//ユーザー情報編集画面プルダウン用、役職リスト
	public List<Department> department(Connection connection) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
        	sql.append("*");
            sql.append(" FROM departments");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Department> depart = todepartment(rs);
            return depart;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<Department> todepartment(ResultSet rs) throws SQLException {

	    List<Department> depart = new ArrayList<>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String name = rs.getString("name");

	            Department department = new Department();
	            department.setId(id);
	            department.setName(name);

	            depart.add(department);
	        }
	        return depart;
	    } finally {
	        close(rs);
	    }
	}

	//ユーザー情報編集画面用SQL文
	public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  account = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", department_id = ?");
            if (!StringUtils.isEmpty(user.getPassword())) {
                sql.append(", password = ?");
            }
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getBranch_id());
            ps.setString(4, user.getDepartment_id());
            if (!StringUtils.isEmpty(user.getPassword())) {
	            ps.setString(5, user.getPassword());
	            ps.setInt(6, user.getId());
            } else {
            	ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                try {
					throw new NoRowsUpdatedRuntimeException();
				} catch (NoRowsUpdatedRuntimeException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	//アカウントの停止・復活
	public void isDeleted(Connection connection, int is_deleted, int id) {

        PreparedStatement ps = null;
        try {
//        	if(){
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append(" is_deleted = ?");
	            sql.append(" WHERE");
	            sql.append(" id = ?");

	            ps = connection.prepareStatement(sql.toString());
	            ps.setInt(1, is_deleted);
	            ps.setInt(2, id);
//        	}else{
	            System.out.println(ps);
        		int count = ps.executeUpdate();
	            if (count == 0) {
	                try {
						throw new NoRowsUpdatedRuntimeException();
					} catch (NoRowsUpdatedRuntimeException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
	            }
//        	}
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}