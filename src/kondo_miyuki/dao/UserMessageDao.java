package kondo_miyuki.dao;

import static kondo_miyuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import kondo_miyuki.bean.UserMessage;
import kondo_miyuki.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, String start, String end, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("contributions.id as id, ");
            sql.append("contributions.title as title, ");
            sql.append("contributions.text as text, ");
            sql.append("contributions.category as category, ");
            sql.append("contributions.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as user_name, ");
            sql.append("contributions.created_date as created_date ");
            sql.append("FROM contributions ");
            sql.append("INNER JOIN users ");
            sql.append("ON contributions.user_id = users.id ");
            sql.append("WHERE ");
            sql.append("contributions.created_date >= ? ");//start
            sql.append("AND contributions.created_date <= ? ");//end
            if(category != null && !StringUtils.isEmpty(category)){
            	sql.append("AND category LIKE ?");
            }
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, start);
            ps.setString(2, end);
            if(category != null && !StringUtils.isEmpty(category)){
            	ps.setString(3, "%" + category + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
//                String account = rs.getString("account");
//                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                String title = rs.getString("title");
                String category = rs.getString("category");
                String userName = rs.getString("user_name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage contributions = new UserMessage();
                contributions.setId(id);
                contributions.setTitle(title);
                contributions.setText(text);
                contributions.setCategory(category);
                contributions.setuser_name(userName);
                contributions.setCreated_date(createdDate);

                ret.add(contributions);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}