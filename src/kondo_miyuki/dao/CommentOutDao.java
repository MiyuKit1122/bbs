package kondo_miyuki.dao;

import static kondo_miyuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kondo_miyuki.bean.Comment;
import kondo_miyuki.exception.SQLRuntimeException;

public class CommentOutDao {

	public List<Comment> getComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.comment as comment, ");
            sql.append("contribution_id , ");
            sql.append("users.name as user_name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int contribution_id = rs.getInt("contribution_id");
                String comment = rs.getString("comment");
                String user_name = rs.getString("user_name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Comment comments = new Comment();
                comments.setId(id);
                comments.setComment(comment);
                comments.setContribution_id(contribution_id);
                comments.setUser_name(user_name);
                comments.setCreated_date(createdDate);

                ret.add(comments);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
