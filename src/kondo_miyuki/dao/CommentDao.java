package kondo_miyuki.dao;

import static kondo_miyuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import kondo_miyuki.bean.Comment;
import kondo_miyuki.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append(" comment");
            sql.append(", user_id");
            sql.append(", contribution_id");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // comment
            sql.append(", ?"); // user_id
            sql.append(", ?"); // contribution_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getComment());
            ps.setInt(2, comment.getUser_id());
            ps.setInt(3, comment.getContribution_id());
//            ps.setString(4, comment.getCreated_date());
//            ps.setInt(5, comment.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments ");
            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}