package kondo_miyuki.dao;

import static kondo_miyuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import kondo_miyuki.bean.Message;
import kondo_miyuki.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO contributions ( ");
            sql.append("title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", user_id");
//            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getTitle());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM contributions ");
            sql.append("WHERE ");
            sql.append("id = ?");
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

//    public void serch(Connection connection, String created_date) {

//        PreparedStatement ps = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("SELECT * FROM contributions ");
//            sql.append("WHERE created_date ");
//            sql.append("BETWEEN ? ");
//            sql.append("AND ?;");
//            ps = connection.prepareStatement(sql.toString());
//
//            ps.setString(1, created_date);
//            ps.setString(2, created_date);
//
//            ps.executeUpdate();
//
//        } catch (SQLException e) {
//            throw new SQLRuntimeException(e);
//        } finally {
//            close(ps);
//        }
//    }
//
//    private List<UserMessage> serch(ResultSet rs)
//            throws SQLException {
//    List<UserMessage> serch = new ArrayList<UserMessage>();
//    try {
//        while (rs.next()) {
//            int id = rs.getInt("id");
//            Timestamp createdDate = rs.getTimestamp("created_date");
//
//            UserMessage created_date = new UserMessage();
//            created_date.setId(id);
//            created_date.setCreated_date(createdDate);
//
//            serch.add(created_date);
//        }
//        return serch;
//    } finally {
//        close(rs);
//    }
//}
}