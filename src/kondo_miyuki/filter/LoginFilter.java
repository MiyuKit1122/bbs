package kondo_miyuki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kondo_miyuki.bean.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	throws IOException, ServletException{

	    HttpSession session = ((HttpServletRequest)request).getSession(false);

	    String servletPath = ((HttpServletRequest)request).getServletPath();
	    System.out.println(servletPath);
	    if(!servletPath.equals("/login") && !servletPath.endsWith(".css")){

	        if (session == null){
                session = ((HttpServletRequest)request).getSession(true);
                List<String> messages = new ArrayList<String>();
                messages.add("ログインしてください");
                session.setAttribute("errorMessages", messages);
                ((HttpServletResponse)response).sendRedirect("login");
                return;

	        }else{
	            User loginCheck = (User)session.getAttribute("loginUser");

	            if (loginCheck == null){
	                List<String> messages = new ArrayList<String>();
	                messages.add("ログインしてください");
	                session.setAttribute("errorMessages", messages);
	                ((HttpServletResponse)response).sendRedirect("login");
	                return;
	            }
	        }

	    }
	        chain.doFilter(request, response);
	}
	@Override
	public void init(FilterConfig filterConfig) throws ServletException{
	}

	@Override
	public void destroy(){
	}

}
