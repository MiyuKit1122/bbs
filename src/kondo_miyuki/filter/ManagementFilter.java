package kondo_miyuki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kondo_miyuki.bean.User;

@WebFilter(urlPatterns = {"/management", "/setting","/signup"})
public class ManagementFilter implements Filter {

	    @Override
		public void doFilter(ServletRequest request, ServletResponse response,
				FilterChain chain) throws IOException, ServletException{

	            HttpSession session = ((HttpServletRequest)request).getSession(false);

	            if(!((HttpServletRequest)request).getServletPath().equals("/login")){
	            User loginCheck = (User)session.getAttribute("loginUser");

	            if (loginCheck == null){
	            	chain.doFilter(request, response);
	            	return;
	            }else{
	            	int depart = Integer.parseInt(loginCheck.getDepartment_id());

	            		if(depart != 1){
	            			List<String> messages = new ArrayList<String>();
	            			messages.add("指定されたページへアクセスできません");
			                session.setAttribute("errorMessages", messages);
			                ((HttpServletResponse)response).sendRedirect("./");
			                    return;
	            		}
		            }
	            }
	            chain.doFilter(request, response);
	    }

	    @Override
		public void init(FilterConfig filterConfig) throws ServletException{
	    }

	    @Override
		public void destroy(){
	    }

}
