package kondo_miyuki.bean;

import java.util.Date;

public class Comment {
	private int id;
	private String comment;
    private int user_id;
    private String user_name;
    private int contribution_id;
    private Date created_date;

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getContribution_id() {
		return contribution_id;
	}
	public void setContribution_id(int contribution_id) {
		this.contribution_id = contribution_id;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
}
